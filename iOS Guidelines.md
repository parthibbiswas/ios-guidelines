# iOS Guidelines

| Contents                              |
| ------------------------------------- |
| Naming Conventions                    |
| Code Formatting and Linting           |
| Console Logging                       |
| Storyboards vs Programmatic Approach  |
| Code Commenting                       |
| Optionals and weak references		|
| Error handling                        |
| Testing                               |
| Code Architectures and Design Pattern |
| Third Party Library Usage             |
| Merge Conflict Handling               |

## Naming Conventions
### Class, Struct and Enum names
`class`, `struct` and `enum` must always follow Pascal case.
```swift
class someClass { ... }   //❌
struct someStruct { ... } //❌
enum someEnum { ... }     //❌

class SomeClass { ... }   //✅
struct SomeStruct { ... } //✅
enum SomeEnum { ... }     //✅
```

### Variable Names
#### Variable Declaration
Variable names MUST always follow camel case. No snake case, no Pascal case. Thankfully XCode doesn't support kebab-case.
```swift
var ThisIsAVariable = 0             //❌
let this_is_another_variable = 123  //❌

let thisIsHowItShouldBeDone = "ABC" //✅
```

#### Variable Mapping
Often times when we work with API, the API responses, in most cases follow snake case naming convention. Hence while mapping the API responses in Swift, often times due to lack of times we tend to create snake case variables in `struct`/`class`. **THIS IS TO BE AVOIDED AT ALL COSTS**
Instead what you the developer, are expected to do is to use `CodingKeys` in the `struct`/`class`.

Lets say you have the following JSON:
```json
"user_info": {
	"user_name": "Name of the user",
	"user_phone_number": 1234567890,
	"user_email": "xyz@abc.com"
}
```

This is what rookie developers would end up doing:
```swift
//DO NOT DO THIS ❌
struct UserInfo {
	var user_name: String
	var user_phone_number: Int
	var user_email: String
}
```

Instead this is what you are expected to do:
```swift
//CORRECT WAY ✅
struct UserInfo {
	var userName: String
	var userPhoneNumber: Int
	var userEmail: String

	enum CodingKeys: String, CodingKey {
        case userName        = "user_name"
        case userPhoneNumber = "user_phone_number"
        case userEmail       = "user_email"
    }
}
```
This way you maintain the camelCase naming convention.

NOTE: This tiny example was just for demonstration purpose. In real projects, you'll face API responses with complex structures and creating models for them would take quite some time. Thankfully due to advancement in AI, **you can just paste the entire JSON structure in ChatGPT and get the models created for you following the CodingKeys structure. While you're at it, just replace sensitive data in the JSON with some gibberish.**

### Function Names
Just like variable names, the function names must be camelCase.

## Code Formatting and Linting
#### Indentation
Swift code must always follow 4 space convention. Sometimes when the `.swift` file is modified with vim, adding pressing the Tab button would end up giving 8 spaces instead of 4. Unless you're pro with vim, it is generally recommended not to use it when working with `.swift` files.

#### Function with a long list of parameters
There are cases where a function has a long list of parameters, maybe due to existing code or maybe that is how it is declared in the framework. Either way, what you're expected to do is when you utilise these kind of functions, although not mandatory, but try your best to keep one argument per line for the sake of code readability.
```swift
//Not illegal, but definitely a pain to read
func functionWithLoadsOfParameters(param1: Int, param2: String, param3: Int, param4: String, param5: Int, param6: String, param7: Int, param8: String) {
	//Some code
}

//Strive to to this instead
func functionWithLoadsOfParameters(param1: Int,
				   param2: String,
				   param3: Int,
				   param4: String,
				   param5: Int,
				   param6: String,
				   param7: Int,
				   param8: String) {
	//Some code
}
```

#### Default value for parameters in function
When we write a module, often times we end up creating a function with a really long list of parameters.

```swift
class SomeClass {
	func function(param1: Int, param2: Int, param3: String?, param4: String?, param5: String?, param6: String?, param7: String?) { ... } //DON'T DO THIS ❌
}
```

In the above function we see that parameters after `param3` are all optional. So in this state, assuming we had nothing to do with param3-param7, if we called that function it'd look like this:
```swift
let obj = SomeClass()
obj.function(param1: 123, 
	     param2: 456, 
	     param3: nil, 
	     param4: nil, 
	     param5: nil, 
	     param6: nil, 
	     param7: nil)//BAD CODE ❌
```

As you can see, we are forced to include the parameters not required in the function, which in turn makes the codebase unnecessarily crowded.

In these cases, when you're defining the function in the class, you MUST give `nil` default values to the optional parameters.
```swift
class SomeClass {
	//DO THIS ✅
	func function(param1: Int, 
		      param2: Int, 
		      param3: String? = nil, 
		      param4: String? = nil, 
		      param5: String? = nil, 
		      param6: String? = nil, 
		      param7: String? = nil) {
		... 
	}
}
```

When you do this, you are no longer forced to include the optional parameters, which in turn makes the codebase a lot more cleaner.
```swift
let obj = SomeClass()
obj.function(param1: 123, 
	     param2: 456)
//CLEAN CODE ✅
```

This was just for the optional parameters. You can also provide default values to non optional parameters.

#### Linting
Generally, you don't really need linters when working with XCode. But if you're a stickler for one, use [SwiftLint](https://github.com/realm/SwiftLint).

## Console Logging
Logging is an essential part of developing. It helps us understand if our code is working correctly or not. It is very easy to fall into the trap of abusing `print` statements. You must at all costs avoid using `print` statements. Leaving `print` statements in the production app, especially if it prints out sensitive info like email, passwords, etc. may potentially be exposed during man-in-the-middle attacks.
Hence, if you are to log, use `os_log`. If you want to use third party libraries, use either of these:
- [CocoaLumberjack](https://github.com/CocoaLumberjack/CocoaLumberjack)
- [SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver)

## Storyboards vs Programmatic Approach

| Storyboard Pros                     | Storyboard Cons                                                       |
| ----------------------------------- | --------------------------------------------------------------------- |
| 1. You can create a screen quickly. | 1. Becomes an absolute nightmare when a merge conflict occurs.        |
|                                     | 2. Becomes painful to make any modification.                          |
|                                     | 3. If a storyboard contains a lot of screens, XCode slows to a crawl. |
|                                     | 4. Cannot refactor storyboards                                        |

| Programmatic Pros                                     | Programmatic Cons                        |
| ----------------------------------------------------- | ---------------------------------------- |
| 1. Easy to maintain.                                  | 1. Takes a long time to create a screen. |
| 2. Easy to refactor.                                  |                                          |
| 3. Easy to make modification.                         |                                          |
| 4. Doesn't slow down XCode as quickly as storyboards. |                                          |
| 5. Easier to fix merge conflicts                      |                                          |

As you can see, programmatic approach's pros far outweigh the cons, as compared to its storyboard counterpart.
Hence, when working on a project, **you MUST adhere to programmatic approach**, no matter how much time it takes. This way you'll learn how to maintain code better and also the fancy architectures which you see mentioned in development sphere slowly begin to make sense. You will thank yourself in the future.

## Code Commenting
This section will be broken into 3 parts:
- Documentation
- Code navigation

### Documentation
It is imperative to document your code, even more when complex logic is involved. Documenting code in Xcode involves some basic knowledge of markdown. 

While documenting, you must provide these details:
- Role of parameters in the function (by using **- Parameters** tag)
- What the function returns (by using **- Returns** tag)
- What the function can throw (by using **- Throws** tag)

#### What happens if you don't document your code
If you try to find more info about a function you want to use, you'd end up seeing this:

![](/images/Undocumented_code.png)

Although, the function name is quite self explanatory, we really don't know the purpose of `nameOfFile` parameter.

#### What happens if you document your code
Taking the above example, this is what documented code would look like:

![](/images/Documented_code.png)

Now we know what the purpose of `nameOfFile` is.

To know more about documenting, click [here](https://developer.apple.com/documentation/xcode/writing-documentation).
### Code navigation
For the purpose of navigating the codebase, use these comments:
- //MARK:
- //TODO:
- //FIXME:

//MARK is used to give one liner description
//TODO is used to let others(even yourself later on) know what is to be done.
//FIXME is used to let others know that that portion of area is problematic.

When you're on a file, press `Ctrl + 6`. That'll open up code navigator. From there you can jump to the sections where you've marked your code with MARK, TODO, FIXME instead of mindlessly scrolling down. The three tags have distinct icons.

![](/images/Codenavigation.png)

When working in a collaborative environment, this will help your teammates navigate to the concerned area faster.

To know more about this, click [here](https://developer.apple.com/documentation/xcode/creating-organizing-and-editing-source-files#Annotate-your-code-for-visibility).

## Optionals and Weak references
Whenever you encounter optionals, **refrain from force-unwrapping it**.

```swift
//✅ DO THIS
if let optionalVar = someOptionalVariable {
	//Some code
}

//✅ DO THIS
guard let optionalVar = someOptionalVariable else { return }

//❌ DO NOT DO THIS
let optionalVar = someOptionalVar!
```

When working with closures, it is very easy to retain reference to class you're working on. Hence when working with closures(or even DispatchQueue/OperationQueue for that matter), ALWAYS use `[weal self] in`. Otherwise you will set yourself up for potential crash.

```swift
//✅ DO THIS
DispatchQueue.main.async { [weak self] in
	guard let self = self else { return }
	//Some code
}

//✅ DO THIS
OperationQueue.main.addOperation { [weak self] in
	guard let self = self else { return }
	//Some code
}

//✅ DO THIS
SomeClass.shared.someClosure { [weak self] in
	guard let self = self else { return }
	//Some code
}

//✅ DO THIS
let closureVariable = { [weak self] in
	guard let self = self else { return }
	//Some code
}

//❌ CRASH PRONE CODE
DispatchQueue.main.async {
	self.someFunction()
}

//❌ CRASH PRONE CODE
OperationQueue.main.addOperation {
	self.someFunction()
}

//❌ CRASH PRONE CODE
SomeClass.shared.someClosure {
	self.someFunction()
}

//❌ CRASH PRONE CODE
let closureVariable = {
	self.someFunction()
}
```

## Error handling
Handling errors is a crucial part of development. **Make sure you DO NOT include `fatalError()` functions in production code**.

## Testing
Whatever code you write, strive to make it testable. Sometimes due to lack of time or due to urgency to meet deadline, we sacrifice code testability. Because iOS development involves a good combination of design and logical code, **you the developer MUST test the logical part of the codebase so that when there's a major UI redesign in the future, you don't have to worry about the logical part breaking**. With that said, for making the code testable, you must adhere to TDD and stick to architectures like MVVM.
**There is no specific requirement for n% coverage. As long as the logical part is unit tested, you're good.**

## Code Architectures and Design Pattern
Small Projects: MVC
Large Projects: MVVM

You are more than welcome to use Clean/VIPER, but please ensure your teammates know about it. These architectures are quite a hard nut to crack.

Though not necessary, update yourself of various design patterns such as Strategy, Command, Builder, Factory, Facade, etc. Having an idea of design patters will help you in keeping the codebase loosely coupled, isolated and most importantly make the code testable.

## Third Party Library Usage
Usage of third party libraries can surely save a lot of time, but the biggest downside of using them are that it can potentially reduce build and compile times and not just that, it becomes messier if we have to dig in the library to make changes in order to meet business requirements. These two downsides can still be dealt with but the bigger pain point comes up when the app needs to be developed for Android as well and in such cases if the Android counterpart for the library is not available, you pretty much lose the uniformity between the two platforms.

Hence, it is expected of you to first develop the functionality yourself first. This way, not only it becomes easier to communicate with the Android team but also you don't add unnecessary bloat to your app. In addition to this, it also becomes easier to communicate with the team members if any new change requirement comes up.

Use third party libraries only if:
1. You've tried everything and still can't come up with a solution.
2. You're running out of time.
3. You're at your wits' end.

If you've finally decided to use third party libraries, keep this in mind: the library you choose should one which is actively maintained. The only way to know this is to check when was the last time a push was made.

Using a library that hasn't been maintained in a long time will have these downsides:
- Usage of deprecated APIs
- Potential incompatibility with the latest OS version.
- Existence of unsolved bugs

Choose libraries wisely.

## Merge Conflict Handling
XCode provides a very poor support when it comes to handling merge conflicts. If you face a merge conflict, open the file with VSCode. VSCode makes it easier to spot conflicted areas, hence saving time in resolving them.